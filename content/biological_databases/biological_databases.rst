Databases
=========

This lecture covers an overview of publicly available databases that are
used to store biological data.

* `online slides`_
* :download:`pdf slides <databases.pdf>`
* :download:`pptx slides <databases.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTHx-LAMZbtve9yQ41aaLdayG9EQBf9pwWLCFzubAGzbh-h9QpWW6n60gVgvHInyyjkfW3pC6fVSIir/pub?start=false&loop=false&delayms=60000
