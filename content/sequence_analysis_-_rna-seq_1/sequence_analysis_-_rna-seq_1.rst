Sequence Analysis 2 - RNA-Seq 1
===============================

This lecture covers an overview of RNA-Seq library design considerations and
generation.

* `online slides`_
* :download:`pdf slides <RNA_Seq.pdf>`
* :download:`pptx slides <RNA_Seq.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTZa69m_srKP3qkG39hXLPxFj8qxFm7iyXHmafhzw280hwYRcLZC1lfW3D9e_GZMtPyUQXwTKQR6RSk/pub?start=false&loop=false&delayms=60000
