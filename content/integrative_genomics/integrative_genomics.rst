Integrative Genomics
====================

This lecture discusses concepts in integrative genomics.

* `online slides`_
* :download:`pdf slides <integrative_genomics.pdf>`
* :download:`pptx slides <integrative_genomics.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRmNM78YD0Ah8DHuwBwH4JmndxT8MeljqSQvknkyw-p61MNJs7UOt69FbIcu69nFwtTLzEio86WBj-t/pub?start=false&loop=false&delayms=60000
