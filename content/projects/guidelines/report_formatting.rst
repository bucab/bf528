Formatting
==========

Clear and consistent formatting is an important aspect of communicating in writing. At worst, inconsistent formatting can interfere with the effectiveness and clarity of your writing, making it difficult to understand. Everything related to how your text, figures, and tables appear visually on the page is formatting, including font, font size, heading and subheading position and size, location and size of figures and corresponding captions, margin widths, use of color, etc. All sections of a document should be consistent in all of these areas. Consistent formatting can take time and effort when incorporating sources from multiple documents/programs. Programs like Microsoft Word often have specific formatting settings that differ between users. One way to ensure consistent formatting is to use a typesetting program like LaTeX, possibly used with an online editor platform like overleaf.com, to separate the content (i.e. text, images, etc) from the layout.

Common mistakes
+++++++++++++++

- Excessive (>1" each) or miniscule (<0.25" each) margins
- Margins should be consistent throughout
- Section heading and subheading levels do not have consistent font weight and size throughout document
- Inconsistent indentation of blocks of text: 
  * All new paragraphs should have same indentation of first line (may be none)
  * Separation (i.e. blank space) between paragraphs should be consistent
  * Block quotes, if used, should use identical formatting throughout
- Figures and tables should be centered on the page (or column, if using multiple column layouts), and captions centered immediately below them
- Except in section headings, font decorations like underline or bold should generally be avoided
- Citations referenced from the text should have consistent format (e.g. (McKee 2016), or [1])

