Cohesion and Coherence
======================

A cohesive document is consistent in all aspects throughout. This includes not
only formatting, but also grammatical features like tense, voice, and tone, and
stylistic characteristics where sentences, clauses, and ideas flow from one to
the next. Although many published manuscripts have sections that are written by
different people, articles typically (or should) sound as if they were written
by one person. Consistency throughout the document makes it easier to read and
understand. Beyond consistency of an entire document, cohesive writing flows
from sentence to sentence in a logical progression that aids the reader in
comprehension. Each sentence or clause should follow logically from the one
that came before, and set up the reader for the next to come.

Cohesion and coherence are some of the most difficult writing concepts to
master. For more detailed description and discussion of cohesion and coherence,
see `this excellent post`_.

.. _this excellent post: https://eapwriting.com/coherence/
