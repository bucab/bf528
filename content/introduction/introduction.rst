Introduction
============

This lecture covers overview information on the course and a description of
the class and project structure. Results from the survey are also presented
and discussed. The lecture ends with an overview of bioinformatics.

Introductory Slides
-------------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vRCP0p5ARvNRpPo6KzjGdcTvT1SBVoz8W7M6see7n-ricraiwD-VgfLqb-I7OAHjTnMqhrY48iVrlSw/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <introduction.pdf>`
* :download:`pptx slides <introduction.pptx>`

Computational Primer Slides
---------------------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vRJSQzG48GI9cZf4MemUIfXPKJ342k91ZkyioZGRdqiMDkJXYzDsqLNcEyIJkj-bikqn8r0sj8cyBif/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <computational_skills_primer.pdf>`
* :download:`pptx slides <computational_skills_primer.pptx>`

Assignment
----------

Familiarize yourself with the material on basic command line usage found here:

`Workshop 0. Basic Linux and Command Line Usage <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/00_cli_basics/00_cli_basics.html>`_

