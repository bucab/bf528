Computational Pipeline Strategies
=================================

This lecture describes the workflow management software concept and
demonstrates two tools commonly used in Bioinformatics.

* `online slides`_
* :download:`pdf slides <computational_pipeline_strategies.pdf>`
* :download:`pptx slides <computational_pipeline_strategies.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQ_mAwA9Lc43MC9ZeT0vTkc4zuAyIObEybtGh7DE8FOeIpocD9KNtbVuEtK36DCiEw4-2wlR-vZtndr/pub?start=false&loop=false&delayms=60000
