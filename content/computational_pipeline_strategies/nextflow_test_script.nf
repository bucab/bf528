#!/usr/bin/env nextflow
 
/*
 * Defines pipeline parameters in order to specify the refence genomes
 * and read pairs by using the command line options
 */


version = 0.1
params.genome = 'ggal'
params.fasta = params.genome ? params.genomes[params.genome].fasta ?: false : false
params.gtf  = params.genome ? params.genomes[params.genome].gtf ?: false : false
params.reads = params.genomes[params.genome].reads
params.outdir = params.genomes[params.genome].outdir
params.email = params.genome ? params.genomes[params.genome].email ?: false : false





log.info "===================================="
log.info " PIPELINER RNAseq Pipeline v${version}"
log.info "===================================="
log.info "Reads       : ${params.reads}"
log.info "Genome      : ${params.genome}"
log.info "FASTA	      : ${params.fasta}"
log.info "Annotation  : ${params.gtf}"
log.info "Output dir  : ${params.outdir}"
//log.info "Current home   : $HOME"
//log.info "Current user   : $USER"
//log.info "Current path   : $PWD"
log.info "===================================="


/*
 * The reference genome file
 */
genome_file = file(params.fasta)
  
/*
 * Creates the `read_pairs` channel that emits for each read-pair a tuple containing
 * three elements: the pair ID, the first read-pair file and the second read-pair file
 */
Channel
    .fromFilePairs(params.reads)                                             
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }  
    .set { read_pairs }
  
/*
 * Step 1. Builds the genome index required by the mapping process
 */
process buildIndex {
    input:
    file genome from genome_file
      
    output:
    file 'genome.index*' into genome_index
        
    """
    bowtie2-build ${genome} genome.index
    """
}
  
/*
 * Step 2. Maps each read-pair by using Tophat2 mapper tool
 */
process mapping {    
    input:
    file genome from genome_file
    file index from genome_index
    set pair_id, file(reads) from read_pairs
  
    output:
    set pair_id, "tophat_out/accepted_hits.bam" into bam_files
  
    """
    tophat2 genome.index ${reads}
    """
}
  
/*
 * Step 3. Assembles the transcript by using the "cufflinks"
 * and publish the transcript output files into the `results` folder
 */
process makeTranscript {
    publishDir "${params.outdir}", mode: 'copy' 
    input:
    set pair_id, bam_file from bam_files
      
    output:
    set pair_id, 'transcripts.gtf' into transcripts
  
    """
    cufflinks ${bam_file}
    """
}



workflow.onComplete {
        println ( workflow.success ? "CONGRATULATIONS !!!!! Your pipeline executed successfully :) !!" : "Oops .. something went wrong" )
    	def subject = 'My pipeline execution'
    	def recipient = (params.email)

    ['mail', '-s', subject, recipient].execute() << """
    Pipeline execution summary
    ---------------------------
    Completed at: ${workflow.complete}
    Duration    : ${workflow.duration}
    Success     : ${workflow.success}
    workDir     : ${workflow.workDir}
    exit status : ${workflow.exitStatus}
    Error report: ${workflow.errorReport ?: '-'}
    """
}
