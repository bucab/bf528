Computational Strategies
========================

This lecture describes the concepts and strategies for reproducibility
in Bioinformatics.

* `online slides`_
* :download:`pdf slides <computational_environment_management.pdf>`
* :download:`pptx slides <computational_environment_management.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vShkoUtSv3pKEK7TnJnw6LARP7yriWz9TTYnL6mjWNyvnyVLdRc_hriGM6LmiWT-_F-OvRb91qYoiC6/pub?start=false&loop=false&delayms=60000
