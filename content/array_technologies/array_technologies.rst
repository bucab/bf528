Array Technologies
==================

This lecture covers an overview of microarrays and analysis.

Microarrays
-----------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vQ5tOnr9uT3vOu-1Idrc_5FZGvG80vrBggdFQTn_G76zkZAl-LY38HexjCBJR-1BHZqybRNRaWH9aHw/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <microarrays.pdf>`
* :download:`pptx slides <microarrays.pptx>`

git
---

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vRDq6AzJsjbm4O-e5sZmJCLbwLdsVJteD_Vym7xJJOJiZQJKoaD9ezI7u_sYG--TWSekHaV3wys_MLe/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <git.pdf>`
* :download:`pptx slides <git.pptx>`


Project Principles
------------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vSpq4leuEzA-nOxPjY5AaDk02K4At1H0R7DtSGd4vWxSkMtZC1hZcYcQzjiALiI4fC2Ro4lle4ZVQxG/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <project_principles.pdf>`
* :download:`pptx slides <project_principles.pptx>`

Assignment
----------

Read the project 1 description and paper.

:doc:`../projects/project_1_microarrays/project_1_microarrays.rst`

Familiarize yourself with the Workshop 4 on R, up through the Data Wrangling section (and keep going if you want to):

`Workshop 4. Introduction to R <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/04_R/04_R.html>`_

