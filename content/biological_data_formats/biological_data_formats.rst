Biological Data Formats
=======================

This lecture covers an overview of standardized data formats
used to store biological data.

* `online slides`_
* :download:`pdf slides <Biological_Data_Formats.pdf>`
* :download:`pptx slides <Biological_Data_Formats.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTAlHBQ0ciGwBMgDAOLijFpez83CWB89joeVXGCh41M_UYa00I8Ic-rF8paVdGyrk39Mg5je7OL1fNH/pub?start=false&loop=false&delayms=60000