Replicability vs Reproducibility Strategies
===========================================

This lecture describes the concepts and strategies for reproducibility
in Bioinformatics.

* `online slides`_
* :download:`pdf slides <reproducibility.pdf>`
* :download:`pptx slides <reproducibility.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vSoklMcPz61vb1qmozSyJfk1KAcmZX_rZvAiRc-PdiCk1h8vrssgMY9ArH0MxH83K_ph7mmX2aomsT2/pub?start=false&loop=false&delayms=60000
