Metabolomics
============

This lecture introduces the concept of metabolomics and briefly covers
metabolomic data, how it is generated, and how it is analyzed.

* `online slides`_
* :download:`pdf slides <metabolomics.pdf>`
* :download:`pptx slides <metabolomics.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQhRAdSzUxWjR1B224cMrqoSzG1mnPNonoqwYtjOX8Jg3rwr1VIbwc70Sejo3TKCTTkyircMZJox388/pub?start=false&loop=false&delayms=3000
