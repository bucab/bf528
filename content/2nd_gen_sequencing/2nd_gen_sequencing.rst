2nd Gen Sequencing
==================

This lecture covers an overview of second generation sequencing
data.

* `online slides`_
* :download:`pdf slides <2nd_generation_sequencing.pdf>`
* :download:`pptx slides <2nd_generation_sequencing.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTzyfENJGLok-iFfBGnQJsLkxgi85tiRYFPo1SMEs6QMRgdRuhlglLwxOz3MrRc_Ne26chdT3TY3d6X/pub?start=false&loop=false&delayms=60000
