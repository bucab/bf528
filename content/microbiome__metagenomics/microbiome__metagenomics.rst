Microbiome: Metagenomics
========================

This lecture introduces the concept of the metagenome and how we can use
sequence information to learn about microbial ecologies.

* `online slides`_
* :download:`pdf slides <microbiome_metagenomics.pdf>`
* :download:`pptx slides <microbiome_metagenomics.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQnnx58Hgxn8jmE1lJizH3A_8NUCZ9_BOdUGHrr9GySVcyL5zJUXTsMorSb6j8PC8SQdnOEn5yl7KdC/pub?start=false&loop=false&delayms=60000
