Sequence Analysis 3 - Single Cell Techniques
============================================

This lecture introduces the concept of single cell sequencing technology.

* `online slides`_
* :download:`pdf slides <single_cell_sequencing.pdf>`
* :download:`pptx slides <single_cell_sequencing.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vRmDn8Wccs3U-ackOYlQEKerZYlb7LZURl36H7WOUXDI3uZMc2Wbv5wYEb4_p5yISuemH4pd3rnTAf_/pub?start=false&loop=false&delayms=60000
