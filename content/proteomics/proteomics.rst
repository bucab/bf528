Proteomics
==========

This lecture covers an overview of proteomics methods.

* `online slides`_
* :download:`pdf slides <proteomics.pdf>`
* :download:`pptx slides <proteomics.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vSZYrZAckcu3gOhagpRVUChw7YP0b4y8B4BxU2ghgNdLBiCSXHEGDYweeJDIUhnXP8qIaupPpTJozCN/pub?start=false&loop=false&delayms=60000


Docker
======

* `docker`_
* :download:`pdf slides <docker.pdf>`
* :download:`pptx slides <docker.pptx>`

.. _docker: https://docs.google.com/presentation/d/e/2PACX-1vRLilR08I6jXh1bv1MxrTD5ZaQiJ2MRxfIVuDV3Hgj1P5OmQIxA8r9Bscv73-Arsi9-hmwrAr2nHrAL/pub?start=false&loop=false&delayms=60000
