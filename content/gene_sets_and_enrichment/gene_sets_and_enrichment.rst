Gene sets and enrichment
========================

This lecture describes the concept of gene sets and gene set enrichment
methods.

Gene sets and enrichment
------------------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vSbmjCME7ucFrniZ1QMpK8P2hirfWNcHvBkGuuQnWxq2vdLDDFlquqB4GcMFGnKT39HyX1PNzZejH_h/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <gene_sets_and_enrichment.pdf>`
* :download:`pptx slides <gene_sets_and_enrichment.pptx>`

R and RStudio Primer
--------------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vQI2jy5w0llgl_rUZUxLBBm2yQbdT3k4Zd2Et9D6S3_DBa_avsGTm4Fd-DowmgTKr5dOF60PXp4rpLI/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <r_and_rstudio.pdf>`
* :download:`pptx slides <r_and_rstudio.pptx>`
