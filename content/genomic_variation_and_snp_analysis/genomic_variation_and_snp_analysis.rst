Genomic Variation and SNP Analysis
==================================

This lecture covers an overview genomic variants and sequencing analysis
techniques for identifying them.

* `online slides`_
* :download:`pdf slides <genomic_variation_and_SNP_Analysis.pdf>`
* :download:`pptx slides <genomic_variation_and_SNP_Analysis.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vSlxVUED6_ewePCkRq-KW94rlms2XY40aIUD3hVxQM6zgXoyTGMa4Aq4XwZHQd6Ub52prTmrWVqhOUG/pub?start=false&loop=false&delayms=3000
