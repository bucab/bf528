Sequence Visualization
======================

This lecture describes tools that can visualize alignments in a human friendly
format, as well as integrate other sources of information for interpretation
of sequencing datasets.

* `online slides`_
* :download:`pdf slides <seq_vis.pdf>`
* :download:`pptx slides <seq_vis.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQ2EG2cOeuX_m6GbOLy0xRiO0M6npga70kFQpEwbV6L4iMRH6zdw9KF1-mc76eoKDMhMuCfMG9nsdBj/pub?start=false&loop=false&delayms=60000
