Genomics, Genes, and Genomes
============================

This lecture covers an overview of the human genome, the concepts of genes
and gene expression, and how we interrogate them.

Genes and Genomes
-----------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vRTcNda7r9JX_oLmuzNmpOPyUqLoQtullVFEj57KWQfbkl5X6XY6dCoHbGu2v3yuFWNp6msFeASLnQ1/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <genetics_and_genomics.pdf>`
* :download:`pptx slides <genetics_and_genomics.pptx>`

SCC Cluster Usage
-----------------

* `online slides <https://docs.google.com/presentation/d/e/2PACX-1vROPYHWi3Ec0sn5oqEfEkKKLOVsegrlI0qdK2cWxX_dRSi76tibE3dhHrDyGKLHBs4U0qj6Igh4yGfh/pub?start=false&loop=false&delayms=60000>`_
* :download:`pdf slides <scc_cluster_usage.pdf>`
* :download:`pptx slides <scc_cluster_usage.pptx>`

Assignment
----------

Familiarize yourself with the material on bash scripting and cluster computing
found here:

`Workshop 6. Shell Scripts and Cluster Computing <http://foundations-in-computational-skills.readthedocs.io/en/latest/content/workshops/06_cluster_computing/06_cluster_computing.html>`_
