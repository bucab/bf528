Sequence Analysis 4 - ChIP-Seq
==============================

This lecture describes ChIP-Seq experiments, methodology, and analysis.

* `online slides`_
* :download:`pdf slides <chip-seq.pdf>`
* :download:`pptx slides <chip-seq.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vTn4TYtzS6V5Otdwk21kkEcpJYJT85QSKI9WyV02jg4eocvZ0DIoLCOO4Hc-JXG6e26myuMmVeeCqd4/pub?start=false&loop=false&delayms=60000
