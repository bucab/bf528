Microbiome: 16S
===============

This lecture introduces the concept of the microbiome and how we use the
ribosomal subunit 16S to characterize a population of microbes.

* `online slides`_
* :download:`pdf slides <microbiome_16S.pdf>`
* :download:`pptx slides <microbiome_16S.pptx>`

.. _online slides: https://docs.google.com/presentation/d/e/2PACX-1vQt0DvUo-HvVBKEowigxFLoAQzh1-zaULnUTntOxVLIUVXzdjWlx-187v7JRshgDnPCNRlkFZ7XIhXx/pub?start=false&loop=false&delayms=60000
