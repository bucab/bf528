'''
This script takes the contents of schedule.csv and produces a new file 
schedule_fmt.csv file with Topic contents formatted as ReST links.

It also creates a schedule_toc.rst file that contains the toctree for the
appropriate files

It also creates the skeleton of the course under the content/ directory,
creating .rst files for each topic if they don't already exist.
'''
import calendar
import csv
from datetime import datetime
import io
import os
from pathlib import Path

import urllib.request

url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVpQJcQW_-HbMB1B87II15RSfO2xp0-0ytMKq8We8K-xcwi9gQK7PWUUQplmCdgH3Be2ktd1gWGtsS/pub?gid=0&single=true&output=csv'

doc_dir = 'content'
if not os.path.isdir('content') :
    os.mkdir(doc_dir)

# change this year appropriately for the semester so the day of week is right
year = 2022

field_names = 'Lecture','Date','Topic','Secondary','online','outline','in-class'
with open('schedule_fmt.csv','w') as out_f :
    response = urllib.request.urlopen(url)
    resp = io.StringIO(response.read().decode())
    out_f = csv.writer(out_f)
    for i,rec in enumerate(csv.DictReader(resp)) :
        num, date, topic, secondary, online, outline, inclass = [rec[_] for _ in field_names]

        dayname = ''
        if date != '' :

          date_obj = datetime.strptime(date,'%m/%d/%Y')
          dayname = calendar.day_abbr[date_obj.weekday()]

        # no num means no class
        if num == '' :
          out_f.writerow([
            num,
            dayname,
            date,
            topic,
            secondary,
            rec['Project']
          ])
          continue

        safe_topic = topic.lower().translate(str.maketrans(' :/,','____'))

        # create the content dir if it doesn't exist
        dir_base = '{}'.format(safe_topic)
        dirname = Path(doc_dir).joinpath(dir_base)
        #dirname.mkdir(exist_ok=True,parents=True)
        print(dirname)

        # create the files if they don't exist
        rst_path = dirname.joinpath(safe_topic+'.rst')
        if online == '1' :
            topic = ':doc:`{} <{}>`'.format(
              topic,
              rst_path.with_suffix('')
            )

        out_f.writerow([
            num,
            dayname,
            date,
            topic,
            secondary,
            rec['Project']
        ])
