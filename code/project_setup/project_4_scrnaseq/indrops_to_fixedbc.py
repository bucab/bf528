from Bio import SeqIO
from collections import Counter
import gzip
from itertools import combinations, product
import numpy
from pprint import pprint
import sys
import re
import requests


def ind_slices(inds) :
    inds = list(inds)
    if 0 not in inds :
        inds.append(0)
    inds = sorted(inds)
    inds.append(None) # for the end of the list
    slices = []
    for i,j in zip(inds[:-1],inds[1:]) :
        slices.append(slice(i,i+1))
        if i+1 != j :
            slices.append(slice(i+1,j))

    return slices

nucs = list('ACGTN')
def create_neighborhood(seq,dist=2) :
    n = []
    for inds in combinations(range(len(seq)),dist) :
        slices = ind_slices(inds)
        prod_parts = [nucs if _.start in inds else [seq[_]] for _ in slices]
        prod_seqs = list(product(*prod_parts))
        n.extend([''.join(_) for _ in prod_seqs])
    return set(n)

w1 = "GAGTGATTGCTTGTGACGCCTT"
w1_neighborhood = create_neighborhood(w1,2)

bc1_map = {}
bc1_url = 'https://raw.githubusercontent.com/indrops/indrops/master/ref/barcode_lists/gel_barcode1_list.txt'
with requests.get(bc1_url) as r :
    for bc in r.text.strip().split('\n') :
        bc = bc.strip()
        bc1_map.update({_:bc for _ in create_neighborhood(bc,2)})

bc2_map = {}
bc2_url = 'https://raw.githubusercontent.com/indrops/indrops/master/ref/barcode_lists/gel_barcode2_list.txt'
with requests.get(bc2_url) as r :
    for bc in r.text.strip().split('\n') :
        bc = bc.strip()
        bc2_map.update({_:bc for _ in create_neighborhood(bc,2)})

bc_fn, freq_bc_fn = sys.argv[1:]

stats = Counter()

bc1_counter = Counter()
bc2_counter = Counter()
bc_counter = Counter()

fout = sys.stdout

with gzip.open(bc_fn,'rt') as f :
    for sh in f :
        seq, qh, qual = [next(f).strip() for _ in (0,)*3]
        stats['reads'] += 1

        rec = None # this gets populated if we found a linker

        # look for the beginning of the linker
        pos = seq.find(w1[:7])
        if pos == -1 :
            # look for the middle
            pos = seq.find(w1[7:14]) -7

            # look for the end
            if pos < 0 :
                pos = seq.find(w1[-7:])-len(w1)+7

        # bc1 must be at least 8 nucs long
        if pos > 7 :
            stats['linker part'] += 1

            linker = seq[pos:pos+len(w1)]
            stats['true linker reads'] += linker == w1

            # check to for full linker sequence or one of its neighbors
            if linker in w1_neighborhood :
                stats['linker reads'] += 1

                bc1 = seq[:pos]
                bc2 = seq[pos+len(w1):pos+len(w1)+8]
                umi = seq[pos+len(w1)+8:pos+len(w1)+8+6]

                qbc1 = qual[:pos]
                qbc2 = qual[pos+len(w1):pos+len(w1)+8]
                qumi = qual[pos+len(w1)+8:pos+len(w1)+8+6]

                alt_bc1 = bc1_map.get(bc1,bc1)
                alt_bc2 = bc2_map.get(bc2,bc2)

                if bc1 != alt_bc1 :
                    stats['alt bc1'] += 1
                if bc2 != alt_bc2 :
                    stats['alt bc2'] += 1

                bc1 = alt_bc1
                bc2 = alt_bc2

                stats['len {}'.format(len(bc1))] += 1
                if len(bc1) == 8 :
                    bc1 += 'ACT'
                    qbc1 += 'III'
                elif len(bc1) == 9 :
                    bc1 += 'AC'
                    qbc1 += 'II'
                elif len(bc1) == 10 :
                    bc1 += 'A'
                    qbc1 += 'I'

                bc1_counter[bc1] += 1
                bc2_counter[bc2] += 1
                bc_counter[bc1+bc2] += 1

                rec = ('{} bc1={} bc2={} umi={}\n'.format(
                            sh.split(' ')[0],bc1,bc2,umi
                        ),
                       '{}{}{}\n'.format(bc1,bc2,umi),
                       '+\n',
                       '{}{}{}\n'.format(qbc1,qbc2,qumi)
                      )

        if rec is None :
            stats['no linker'] += 1
            rec = ('{}\n'.format(sh.split(' ')[0]),
                   '{}\n'.format(seq),
                   '+\n',
                   '{}\n'.format(qual)
                  )
        fout.write(''.join(rec))


pprint(stats,stream=sys.stderr)

pprint(bc_counter.most_common(20),stream=sys.stderr)

print('num unique barcodes: {}'.format(len(bc_counter)),file=sys.stderr)

percentiles = [10,25,75,90,95,99]
percentile_vals = numpy.percentile(list(bc_counter.values()),percentiles)
percentiles = dict(zip(percentiles, percentile_vals))

pprint(percentiles, stream=sys.stderr)

# must have at least 1k reads to be worth anything
frequent_bcs = {k:c for k,c in bc_counter.items() if c > percentiles[95]}
print('num unique barcodes > 95th %ile reads: {}'.format(
        len(frequent_bcs)
    ),
    file=sys.stderr
)

with open(freq_bc_fn,'wt') as f :
    f.write(''.join(_+'\n' for _ in frequent_bcs.keys()))
