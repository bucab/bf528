import csv

samples = {}
with open('SRR_Acc_List.csv') as f :
  for r in csv.reader(f) :
    samples[r[0]] = r[1]

# remove P0_1 so the students have to do it
del samples['P0_1']

projdir = '/project/bf528/project_2'
datadir = '{}/data'.format(projdir)
fastqs = expand('{dir}/samples/{sample}/{sample}_{end}.fastq.gz',
  dir=datadir,
  sample=samples.keys(),
  end=('1','2')
)
tophats = expand('{dir}/samples/{sample}/accepted_hits.bam',
  dir=datadir,
  sample=samples.keys()
)
cufflinks = expand('{dir}/samples/{sample}/genes.fpkm_tracking',
  dir=datadir,
  sample=samples.keys()
)
merged_asm = '{}/merged_asm'.format(datadir)
fpkm_mat = '{}/data/fpkm_matrix.csv'.format(datadir)
rule all:
  input:
    fastqs,
    tophats,
    cufflinks,
    merged_asm,
    '{}/P4_vs_P7_cuffdiff_out'.format(datadir),
    fpkm_mat

rule download:
  output:
    r1='{path}/{sampname}_1.fastq.gz',
    r2='{path}/{sampname}_2.fastq.gz'
  params:
    sra=lambda w: samples[w['sampname']]
  shell:
    '''fastq-dump -v -v --gzip --split-files -O {wildcards.path} {params.sra} &&
    mv {wildcards.path}/{params.sra}_1.fastq.gz {wildcards.path}/{wildcards.sampname}_1.fastq.gz &&
    mv {wildcards.path}/{params.sra}_2.fastq.gz {wildcards.path}/{wildcards.sampname}_2.fastq.gz'''

rule download_ref:
 output:
  fa='{}/reference/mm9.fa'.format(projdir),
 params:
 shell:
   '''curl ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_mouse/release_M1/NCBIM37.genome.fa.gz |
   gunzip -c > {output.fa}
   '''

rule index_ref:
  input: fa='{}/reference/mm9.fa'.format(projdir),
  output: fai='{}/reference/mm9.fa.fai'.format(projdir)
  shell:
    '''samtools faidx {input.fa}'''

rule bowtie_index:
  input: fa=rules.download_ref.output.fa
  output:
    bt2='{}/reference/mm9.1.bt2'.format(projdir)
  params:
    bt2='{}/reference/mm9'.format(projdir)
  threads: 16
  shell:
    '''bowtie2-build --threads {threads} {input.fa} {params.bt2}'''

rule tophat:
  input:
    r1='{path}/{sampname}/{sampname}_1.fastq.gz',
    r2='{path}/{sampname}/{sampname}_2.fastq.gz',
    index='{}/reference/mm9.1.bt2'.format(projdir),
    gtf='{}/reference/annot/mm9.gtf'.format(projdir)
  output: '{path}/{sampname}/accepted_hits.bam'
  params:
    index='{}/reference/mm9'.format(projdir),
  threads: 16
  shell:
    '''
    # tophat requires python2, but the bf528 conda env requires python3
    # load the module to make tophat available on SCC
    # this step is not reproducible X(
    set +euo pipefail;
    source /etc/bashrc;
    module load python/2.7.13 boost/1.58.0 samtools bowtie2 tophat;
    tophat -p {threads} -r 200 -G {input.gtf} --segment-length=20 \
    --segment-mismatches=1 --no-novel-juncs -o {wildcards.path}/{wildcards.sampname}/ \
    {params.index} {input.r1} {input.r2}
    '''

rule cufflinks:
  input:
    bam='{path}/{sampname}/accepted_hits.bam',
    fa='{}/reference/mm9.fa'.format(projdir),
    fai='{}/reference/mm9.fa.fai'.format(projdir),
    gtf='{}/reference/annot/mm9.gtf'.format(projdir)
  output:
    fpkm='{path}/{sampname}/genes.fpkm_tracking'
  threads: 16
  shell:
    '''set +euo pipefail;
    source /etc/bashrc;
    module load cufflinks;
    cufflinks --compatible-hits-norm -b {input.fa} -p {threads} -u -G {input.gtf} \
    -o {wildcards.path}/{wildcards.sampname} {input.bam}
    '''

rule cuffmerge:
  input:
    cuff=cufflinks,
    gtf='{}/reference/annot/mm9.gtf'.format(projdir),
    fa='{}/reference/mm9.fa'.format(projdir),
  output: merged_asm
  params:
    projdir=projdir,
    datadir=datadir,

  threads: 24
  shell:
    '''
    set +euo pipefail;
    source /etc/bashrc;
    module load cufflinks;
    FN={params.projdir}/all_cufflinks_transcripts.txt
    ls {params.datadir}/samples/*/transcripts.gtf > $FN
    cuffmerge -p {threads} -g {input.gtf} -s {input.fa} -p {threads} -o {output} $FN
    '''

rule cuffdiff:
  input:
    r11='{path}/data/samples/{samp1}_1/accepted_hits.bam',
    r12='{path}/data/samples/{samp1}_2/accepted_hits.bam',
    r21='{path}/data/samples/{samp2}_1/accepted_hits.bam',
    r22='{path}/data/samples/{samp2}_2/accepted_hits.bam',
    asm=merged_asm
  output: '{path}/{samp1}_vs_{samp2}_cuffdiff_out'
  threads: 16
  shell:
    '''set +euo pipefail;
    source /etc/bashrc;
    module load cufflinks;
    cuffdiff -p {threads} -L {wildcards.samp1},{wildcards.samp2} -o {output} \
      {input.asm}/merged.gtf {input.r11},{input.r12} {input.r21},{input.r22}
    '''

rule concat_fpkm:
  input: cufflinks
  output: fpkm_mat
  run:
    import pandas
    dfs = []
    cols = []
    for fn in sorted(cufflinks) :
      sample = fn.split('/')[-2]
      cols.append('{}_FPKM'.format(sample))
      df = pandas.read_table(fn,sep='\t',index_col=0)
      df = df.loc[sorted(df.index)]
      df.rename(columns=dict((_,sample+'_'+_) for _ in df.columns),inplace=True)
      dfs.append(df)

    mat = pandas.DataFrame(index=dfs[0].index,columns=cols)
    for col,df in zip(cols,dfs) :
      mat[col] = df[col]

    mat.to_csv(output[0],sep='\t')
