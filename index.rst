.. BF528 - Applications in Translational Bioinformatics documentation master file, created by
   sphinx-quickstart on Wed Dec 27 13:31:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BF528 - Applications in Translational Bioinformatics
====================================================

Welcome to the homepage of BF528.

**Meeting time:** Monday/Friday 10:10-11:55

**Location:** `EPC 207`_ and Zoom

**Office hours:**
By appointment

.. _EPC 207: https://maps.bu.edu/?id=647#!m/289251?ct/

.. contents:: Table of Contents
   :local:
   :backlinks: top

Description
-----------

The objective of this course is expose students to the topics and technologies
used in modern bioinformatics studies. The course covers a mix of biological
and computational topics, including:

* High throughput genomics techniques (microarrays, 2nd generation sequencing)
* Current high throughput sequencing assays (DNA-Seq, RNA-Seq, ChIP-Seq)
* Differential gene expression techniques
* Microbiome/metagenomics techniques
* Metabolomics
* Proteomics
* Systems, network, and integrative biology
* Basic linux cluster usage
* Python and R scripting
* Computational workflow and replication strategies
* Genomics data visualization techniques
* Biological databases

This is highly hands-on course, where a portion of in-class periods are
dedicated to concerted group work and interactive discussions. The course
materials are focused on real-world applications of the high throughput
genomics techniques and organized into structured **group projects**. Students
are organized into groups of three or four for the entire semester, where they
will work together to replicate results from published studies. The tasks for
each project have been divided into four ‘roles’:

**Data curator**: identify, download, and describe relevant datasets and
literature
   
**Programmer**: write code to transform the downloaded data into an
interpretable form
   
**Analyst**: examine and visualize processed data to aid in interpretation
   
**Biologist**: connect the processed data into meaningful biological
interpretation

Each group will complete four projects over the course of the semester, and for
each project the roles of the members rotate, such that each member fulfills
each role once. Each group will produce a full report describing their work on
each project, including biological background, methods, results, and
interpretation.

The first half of each in-class period will be dedicated to interactive
discussions on special topics germane to the project material. The second half
is dedicated to either special topics as indicated in the schedule below, or
to discussion of the projects, sharing ideas, and communicating challenges both
within and between groups, with the assistance of the instructors.

There are no homeworks or exams, only projects.

Course Values, and Policies
---------------------------

**Everyone is welcome.** Every background, race, color, creed, religion, ethnic origin,
age, sex, sexual orientation, gender identity, nationality is welcome and celebrated
in this course. Everyone deserves respect, patience, and kindness. *Disrespectful language,
discrimination, or harassment of any kind are not tolerated, and may result in removal
from class or the University.* This is not merely `BU policy`_. The instructors deem
these principles to be inviolable human rights. Students should feel safe reporting any
and all instances of discrimination or harassment to the instructor, to any of the
Bioinformatics Program leadership, or the BU `Equal Opportunity Office`_.

.. _BU policy: http://www.bu.edu/policies/policy-category/harassment-discrimination/
.. _Equal Opportunity Office: http://www.bu.edu/eoo/

**Everyone brings value.** Each of us brings unique experiences, skills, and
creativity to this course. Our diversity is our greatest asset.

**Collaboration is highly encouraged**. All students are encouraged to work together and seek
out any and all available resources when completing projects in all aspects of the course,
including sharing both ideas and code as well as those found on the internet. Any and all
available resources may be brought to bear. *However, consistent with BU policy, your reports
should be written in your own words and represent your own work and understanding of the
material.*

**A safe space for dissent.** For complex topics such as those covered in this class, there
is seldom one correct answer, approach, or solution. Disagreement fosters innovation.
All in the course, including students and TAs, are encouraged to express constructive
criticism and alternative ideas on any aspect of the content.

**We are always learning.** Our knowledge and understanding is always incomplete. Even experts
are fallible. The bioinformatics field evolves rapidly, and "Rome was not built in a day."
Be kind to yourself and to others. You are always smarter and more knowledgable today than you
were yesterday.

Prerequisites
-------------

Basic understanding of biology and genomics. Any of these courses are adequate
prerequisites for this course: BF527, BE505/BE605. Students should have some
experience programming in a modern programming language (R, python, C, Java, etc).

Instructor
----------

Adam Labadorf

My pledge to foster Diversity, Inclusion, Anti-racism
+++++++++++++++++++++++++++++++++++++++++++++++++++++

This course is a judgement free and anti-racist learning environment. Our cohort consists of
students from a wide variety of social identities and life circumstances. Everyone will treat
one another with respect and consideration at all times or be asked to leave the classroom.

As instructor, I pledge to

1. Learn and correctly pronounce everyone’s preferred name/nickname
2. Use preferred pronouns for those who wish to indicate this to me/the class
3. Work to accommodate/prevent language related challenges (for instance I will do my best to avoid the use of idioms and slang)

Project Overview
----------------

.. toctree::
  :hidden:
  :glob:

  content/projects/*/*
  content/projects/project_reports

The four projects are as follows:

1. :doc:`content/projects/project_1_microarrays/project_1_microarrays`
2. :doc:`content/projects/project_2_rnaseq_1/project_2_rnaseq_1`
3. :doc:`content/projects/project_3_rnaseq_2/project_3_rnaseq_2`
4. :doc:`content/projects/project_4_scrnaseq/project_4_scrnaseq`
5. :doc:`content/projects/individual_project`

In the individual project, you will choose (at least) two roles from any of the
projects *that you did not previously do*. This will give you an opportunity to
gain experience with tools and skills you may have missed while playing other
roles.

The roles in each project require varying amounts of effort. Since you must do
each role exactly once, below is information about the rating of each role in
each project with respect to the amount of computational skill/time commitment
required to help you choose your sequence of roles:

+--------------+-----------------+-----------------+-----------------+-----------------+
| Role         | Project 1       | Project 2       | Project 3       | Project 4       |
+==============+=================+=================+=================+=================+
| Data Curator | |x| |o| |o| |o| | |x| |x| |o| |o| | |x| |x| |o| |o| | |x| |x| |x| |X| |
+--------------+-----------------+-----------------+-----------------+-----------------+
| Programmer   | |x| |o| |o| |o| | |x| |x| |o| |o| | |x| |x| |x| |o| | |x| |x| |o| |o| |
+--------------+-----------------+-----------------+-----------------+-----------------+
| Analyst      | |x| |x| |o| |o| | |x| |o| |o| |o| | |x| |x| |x| |o| | |x| |x| |x| |o| |
+--------------+-----------------+-----------------+-----------------+-----------------+
| Biologist    | |x| |o| |o| |o| | |x| |o| |o| |o| | |x| |o| |o| |o| | |x| |x| |o| |o| |
+--------------+-----------------+-----------------+-----------------+-----------------+
| |x| |o| |o| |o| -- least skill required;    |x| |x| |x| |X| -- most skill required   |
+--------------+-----------------+-----------------+-----------------+-----------------+

.. |x| unicode:: U+026AB .. black circle
.. |X| unicode:: U+1F534 .. red circle
.. |o| unicode:: U+026AA .. white circle

Taking these ratings into account, we provide four possible sequences for
each member of each group. Sequence 1 through 4 involve increasingly difficult
computational tasks:

+--------------+--------------+--------------+--------------+--------------+
|              | Project 1    | Project 2    | Project 3    | Project 4    |
+==============+==============+==============+==============+==============+
| Sequence 1   | Programmer   | Analyst      | Data Curator | Biologist    |
+--------------+--------------+--------------+--------------+--------------+
| Sequence 2   | Data Curator | Biologist    | Analyst      | Programmer   |
+--------------+--------------+--------------+--------------+--------------+
| Sequence 3   | Biologist    | Data Curator | Programmer   | Analyst      |
+--------------+--------------+--------------+--------------+--------------+
| Sequence 4   | Analyst      | Programmer   | Biologist    | Data Curator |
+--------------+--------------+--------------+--------------+--------------+

These are only suggestions. These suggested sequences also only reflect the
computational skill involved in each role; those with less biological
background may find the biologist roles more conceptually challenging.

Project guidelines for group reports (1, 2, 3, and 4) are here:
:doc:`content/projects/project_reports`

Grading and Assessment
----------------------

Group report assessment will be the same for all group members. Assessment is
made based on the process completed by the group, not whether the study results
were successfully replicated or not. Each students' grade may be adjusted based
upon the quality of their final individual project relative to their group
reports.

Schedule
--------

**Important:** Note that some weeks have a secondary topic and others do not.
In weeks with a secondary topic, class is split into two lectures. In weeks
without a secondary topic, the second half of class is allocated for you to
work together in your groups/roles and with your TA. **Days without a secondary
topic will be delivered via zoom only to facilitate these interactions.**

.. csv-table::
   :header: "Class", "Day", "Date", "Topic", "Secondary", "Project Out/Due"
   :file: schedule_fmt.csv
   :align: center


Site Index
----------

.. toctree::
   :maxdepth: 2
   :glob:
   
   content/*/*


