# -*- coding: utf-8 -*-
"""
    ReST directive for creating an iframe

    Both directives have three optional arguments: ``height``, ``width``
    and ``align``. Default height is 281 and default width is 500.

    Example::

        .. iframe:: http://www.google.com
            :height: 315
            :width: 560
            :align: left

    :copyright: (c) 2012 by Danilo Bargen.
    :license: BSD 3-clause
"""
from __future__ import absolute_import
from docutils import nodes
from docutils.parsers.rst import Directive, directives


def align(argument):
    """Conversion function for the "align" option."""
    return directives.choice(argument, ('left', 'center', 'right'))


class Iframe(Directive):
    has_content = False
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        'height': directives.nonnegative_int,
        'width': directives.nonnegative_int,
        'align': align,
    }
    default_width = 480
    default_height = 389

    html = '<iframe src="%(url)s" \
    width="%(width)u" height="%(height)u" frameborder="0" \
    webkitAllowFullScreen="true" mozallowfullscreen="true" allowfullscreen="true" \
    class="align-%(align)s"></iframe>'

    def run(self):
        self.options['url'] = directives.uri(self.arguments[0])
        self.options['width'] = self.options.get('width', self.default_width)
        self.options['height'] = self.options.get('height',self.default_height)
        self.options['align'] = self.options.get('align','left')
        return [nodes.raw('', self.html % self.options, format='html')]

# subclass, maybe for later
class Youtube(Iframe):
    has_content = False
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        'height': directives.nonnegative_int,
        'width': directives.nonnegative_int,
        'align': align,
    }
    default_width = 480
    default_height = 389

    html = '<iframe src="http://www.youtube.com/embed/%(video_id)s" \
    width="%(width)u" height="%(height)u" frameborder="0" \
    webkitAllowFullScreen="true" mozallowfullscreen="true" allowfullscreen="true" \
    class="align-%(align)s"></iframe>'

    def run(self):
        self.options['video_id'] = directives.uri(self.arguments[0])
        self.options['width'] = self.options.get('width', self.default_width)
        self.options['height'] = self.options.get('height',self.default_height)
        self.options['align'] = self.options.get('align','left')
        return [nodes.raw('', self.html % self.options, format='html')]

def setup(builder):
    directives.register_directive('iframe', Iframe)
    directives.register_directive('youtube', Youtube)
    #directives.register_directive('vimeo', Vimeo)
